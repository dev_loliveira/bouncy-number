import argparse

parser = argparse.ArgumentParser(description='Bouncy proportion calculator')
parser.add_argument(
	'--proportion', type=int, default=99, help='Least number to reach the specified proportion')


def is_increasing(n):
    str_n = str(n)
    for i in range(1, len(str_n)):
        if str_n[i-1] > str_n[i]:
            return False
    return True

def is_decreasing(n):
    str_n = str(n)
    for i in range(1, len(str_n)):
        if str_n[i-1] < str_n[i]:
            return False
    return True

def is_bouncy(n):
    if not is_increasing(n) and not is_decreasing(n):
        return True
    return False


if __name__ == '__main__':
    args, _ = parser.parse_known_args()
    proportion_to_search = args.proportion
    loop_index = 100
    ibouncy = 0
    result = dict()
    proportion = 0
    i = 0
    while proportion < proportion_to_search:
        if is_bouncy(i):
            ibouncy += 1

        if ibouncy > 0:
            proportion = (ibouncy * 100)/i
            if proportion not in result:
                result[proportion] = (i, ibouncy)
        i += 1

    if proportion_to_search in result:
        print(result[proportion_to_search][0])
    else:
        print('The specified proportion was not found.')
