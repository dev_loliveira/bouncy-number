# Bouncy number

# Installation and execution

  - This application has no external dependencies, so we won't use a requirements.txt nor a virtual environment
  - It can be executed by either Python 2.7+ or Python 3.5+

### Sample commands and outputs

By default, the application will search for the least number to reach the 99% proportion. This behaviour can be changed by passing the '--proportion' flag.
```sh
python bouncy.py -h
python bouncy.py --proportion 50
python bouncy.py --proportion 99
```

| Command | Output |
| ------ | ------ |
| python bouncy.py | 1587000 |
| python bouncy.py --proportion 99 | 1587000 |
| python bouncy.py --proportion 90 | 21780 |
| python bouncy.py --proportion 50 | 538 |

